#
# say_extractSleepStage.R
# 
# <Usage> R --vanilla < say_Visualize.R --args 20111118.ID033572 bin_length
#
#

source("say.R")
library(Cairo)
library("rgl")

args<-commandArgs(trailingOnly = TRUE)
mouse.id<-args[1]
bin.size.in.mins<-as.numeric(args[2])

#mouse.ids<-c("20111118.ID033572", "20111118.ID044502", "20111118.ID045831","20111118.ID051038")
#animal.id<-"ID019917"

load(file.path("data","char", paste(mouse.id, "vols.char","xdr", sep=".")))
load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
pcn<-length(auto.stage$pcs)
auto.stage.stats<-auto.stage[-c(1,2,8)]
auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)

bin.size.in.segments<-(bin.size.in.mins*60)%/%vols.char$segment.size


auto.stage<-matrix(auto.stage, nrow=bin.size.in.segments)

stage.count<-matrix(0, ncol=4, nrow=dim(auto.stage)[2])
for (k in 1:4){
	lb<-sleep.stage.labels[k]
	for (i in sequence(dim(auto.stage)[2])){
		stage.count[i,k]<-length(which(auto.stage[,i]==lb))
	}
}

colnames(stage.count)<-sleep.stage.labels

p<-ggplot(data.frame(time=(0:(length(auto.stage)%/%bin.size.in.segments-1))*bin.size.in.mins/60, 
	NREM=stage.count[,"NREM"]/bin.size.in.segments, 
	Wake=stage.count[,"Wake"]/bin.size.in.segments,
	REM=stage.count[,"REM"]/bin.size.in.segments,
	LD=factor((((0:(length(auto.stage)%/%bin.size.in.segments-1))*bin.size.in.mins/60)%%24)%/%12)))
p<-p+scale_x_continuous(breaks=seq((length(auto.stage)%/%bin.size.in.segments*bin.size.in.mins)%/%(60*6))*6)
p<-p+scale_y_continuous(limits=c(0,1))
p<-p+scale_fill_manual(values=c("orange","black"))

dir.create("figure", recursive=TRUE, showWarnings=FALSE) # make the folders before saving data

for (st in c("NREM","REM", "Wake")){
	CairoPNG(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, "bin-size",
		sprintf("%02d", bin.size.in.mins), "auto.stage", st, "png", sep=".")), width=1200, height=400)
	q<-p+geom_bar(aes_string(x="time", y=st, fill="LD"), stat="identity", width=0.8, alpha=0.8)
	print(q)
	dev.off()
}


write.csv(stage.count, file.path("figure", paste(mouse.id, "bin-size",
	sprintf("%02d", bin.size.in.mins), "auto.stage", "csv", sep=".")))


