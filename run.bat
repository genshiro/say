set m1=ID45172
set m2=ID45255
set m3=ID51207
set m4=ID63820

set sdt=20150224080000

R --vanilla < say_ConvertDSIVolsToXdr.R --args %m1% 100 0 0
R --vanilla < say_ConvertTdVolsToPowVols.R --args %m1% 8 %sdt% 32400
R --vanilla < say_ExtractCharFromPowVols.R --args %m1% 1 0
R --vanilla < say_StageSleep.R --args %m1% 32400 5400 
R --vanilla < say_plotVoltages.R --args %m1% 1 32400
R --vanilla < say_extractSleepStage.R --args %m1% 6
R --vanilla < say_extractSleepStage.R --args %m1% 60


R --vanilla < say_ConvertDSIVolsToXdr.R --args %m2% 100 0 0
R --vanilla < say_ConvertTdVolsToPowVols.R --args %m2% 8 %sdt% 32400
R --vanilla < say_ExtractCharFromPowVols.R --args %m2% 1 0
R --vanilla < say_StageSleep.R --args %m2% 32400 5400 
R --vanilla < say_plotVoltages.R --args %m2% 1 32400
R --vanilla < say_extractSleepStage.R --args %m2% 6
R --vanilla < say_extractSleepStage.R --args %m2% 60


R --vanilla < say_ConvertDSIVolsToXdr.R --args %m3% 100 0 0
R --vanilla < say_ConvertTdVolsToPowVols.R --args %m3% 8 %sdt% 32400
R --vanilla < say_ExtractCharFromPowVols.R --args %m3% 1 0
R --vanilla < say_StageSleep.R --args %m3% 32400 5400 
R --vanilla < say_plotVoltages.R --args %m3% 1 32400
R --vanilla < say_extractSleepStage.R --args %m3% 6
R --vanilla < say_extractSleepStage.R --args %m3% 60

R --vanilla < say_ConvertDSIVolsToXdr.R --args %m4% 100 0 0
R --vanilla < say_ConvertTdVolsToPowVols.R --args %m4% 8 %sdt% 32400
R --vanilla < say_ExtractCharFromPowVols.R --args %m4% 1 0
R --vanilla < say_StageSleep.R --args %m4% 32400 5400 
R --vanilla < say_plotVoltages.R --args %m4% 1 32400
R --vanilla < say_extractSleepStage.R --args %m4% 6
R --vanilla < say_extractSleepStage.R --args %m4% 60
