#
# say_ConvertDSIVolsToXdr_2.R
# 
# <Usage> R --vanilla < say_ConvertDSIVolsToXdr.R --args 20111211.ID044497 part1 part2
#
# conbine two separated dsi.txt files


source("say.R")
log.file<-gen.start.log("DSIVolsToXdr2")
args<-commandArgs(trailingOnly = TRUE)
mouse.id<-args[1]
f1<-"part1"#args[2]
f2<-"part2"#args[3]
sampling.frequency<-100
delta.jst<-0
max.count<-0
#if (length(args)>3) sampling.frequency<-as.numeric(args[4])
#if (length(args)>4) delta.jst<-as.numeric(args[5])
#if (length(args)>5) max.count<-as.numeric(args[6])

print(ConvertDSIVolsToXdr_2(mouse.id, f1, f2, sampling.frequency, delta.jst, max.count, log.file))