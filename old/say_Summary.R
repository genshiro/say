#
# say_Visualize.R
# 
# <Usage> R --vanilla < say_Visualize.R --args 20111118.ID033572
#
#

source("say.R")
library(Cairo)
library("rgl")

args<-commandArgs(trailingOnly = TRUE)
mouse.ids<-args[]

#mouse.ids<-c("20111118.ID033572", "20111118.ID044502", "20111118.ID045831","20111118.ID051038")

df<-matrix(0, ncol=13, nrow=length(mouse.ids))

for (i in seq(length(mouse.ids))){

	mouse.id<-mouse.ids[i]
	load(file.path("data","char", paste(mouse.id, "vols.char","xdr", sep=".")))
	load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
	pcn<-length(auto.stage$pcs)
	auto.stage.stats<-auto.stage[-c(1,2,8)]
	auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
	load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
	manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

	df[i,]<-unlist(CompareStages(auto.stage, manual.stage))
}

df<-data.frame(df)
s<-c("NREM","REM","Wake")
colnames(df)<-c(paste("%",s,sep=""), paste(s,"sens",sep="."), paste(s,"spec",sep="."), paste(s,"acur",sep="."), "Total acuracy")

mean.df<-colMeans(df[])
sd.df<-apply(df[],2,sd)

df<-rbind(df, mean.df, sd.df)
df<-cbind(c(mouse.ids,"mean","sd"), df)
colnames(df)[1]<-"id"

now<-Sys.time()
write.csv(df, file.path("log", paste(format(now,"%Y%m%d%H%M%S"), "summary", "csv", sep=".")))
write("Stats", file.path("log", paste(format(now,"%Y%m%d%H%M%S"), "summary", "csv", sep=".")), append=TRUE)
write(paste("hmult", auto.stage.stats$hmult, sep=","), file.path("log", paste(format(now,"%Y%m%d%H%M%S"), "summary", "csv", sep=".")), append=TRUE)
write(paste("ngrid", auto.stage.stats$n.grid, sep=","), file.path("log", paste(format(now,"%Y%m%d%H%M%S"), "summary", "csv", sep=".")), append=TRUE)
write(paste("pcn", pcn, sep=","), file.path("log", paste(format(now,"%Y%m%d%H%M%S"), "summary", "csv", sep=".")), append=TRUE)
