#
# say_Visualize.R
# 
# <Usage> R --vanilla < say_Visualize.R --args 20111118.ID033572
#
#

source("say.R")
library(Cairo)
library("rgl")

args<-commandArgs(trailingOnly = TRUE)
mouse.id<-args[1]

mouse.ids<-c("20111118.ID033572", "20111118.ID044502", "20111118.ID045831","20111118.ID051038")


load(file.path("data","char", paste(mouse.id, "vols.char","xdr", sep=".")))
load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

stage.color<-c("indianred1","darkolivegreen3","dodgerblue","black")
stage.color.alpha<-c("#ff6a6a66","#a2cd5a66","#1e90ff66", "#00000066")

# calc the eigenvalues

ev.mean<-rep(0, 99)
ev.sd<-rep(0, 99)
sdevs<-data.frame()
ratio.evs<-data.frame()

for (m in seq(length(mouse.ids))){
	load(file.path("data","char", paste(mouse.ids[m], "vols.char","xdr", sep=".")))
	sdevs<-rbind(sdevs, vols.char$sdev[1:99])
	ratio.evs<-rbind(ratio.evs, (vols.char$sdev[1:99]^2)/(sum(vols.char$sdev^2)))
}
colnames(sdevs)<-sprintf("PC%02d", 1:99)
colnames(ratio.evs)<-sprintf("PC%02d", 1:99)

ev.mean<-colMeans(sdevs^2)
ev.sd<-apply(sdevs^2,2,sd)

percent.ev.mean<-colMeans(ratio.evs)
percent.ev.sd<-apply(ratio.evs,2,sd)

CairoFonts(
	regular="Arial:style=Regular",
	bold="Arial:style=Bold",
	italic="Arial:style=Italic",
	bolditalic="Arial:style=Bold Italic,BoldItalic",
	symbol="Symbol"
)


now<-Sys.time()
dir.create("figure", recursive=TRUE, showWarnings=FALSE) # make the folders before saving data
CairoPDF(file.path("figure", paste(format(now,"%Y%m%d%H%M%S"), "eigenvalues_lineplots", "pdf", sep=".")), 8, 8, bg="transparent")

p<-ggplot(data.frame(PC=1:99, mean=percent.ev.mean*100, sd=percent.ev.sd*100))
p<-p+geom_line(aes(y=mean, x=PC))
#p<-p+geom_errorbar(aes(ymax=mean+sd, ymin=mean-sd, x=PC), width=0.2)
p<-p+geom_ribbon(aes(x=PC, ymax=mean+sd, ymin=mean-sd), alpha=0.2)
p<-p+scale_x_continuous("PCs", breaks=c(1:10))
p<-p+ylab("%Eigenvalues (mean±sem)")
p<-p+coord_cartesian(xlim=c(0,10))
p<-p+theme(legend.position = "none", text=element_text(size = 36))	
#p<-p+opts(title=paste(mouse.id, ":", vol.types[i]))
print(p)

dev.off()	

print(rowSums(ratio.evs[,1:4])*100)
print(mean(rowSums(ratio.evs[,1:4]))*100)
print(sd(rowSums(ratio.evs[,1:4]))*100)
print(sd(rowSums(ratio.evs[,1:4]))/sqrt(4)*100)


