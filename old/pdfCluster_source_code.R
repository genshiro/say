# > getMethod("pdfCluster", "matrix")
# Method Definition:

source("say.R")
library(Cairo)
library("rgl")
library("ggplot2")
library("reshape")
library(xtable)
library(knitr)
library(geometry)

mouse.id<-c("20111118.ID044502")#, "20111118.ID044502", "20111118.ID045831","20111118.ID051038")

# load the manual staged data
load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep="."))) # this will load auto.stage

# load vols.char file
load(file.path("data","char", paste(mouse.id, "vols.char.xdr", sep="."))) # this will load vols.char
sample.count<-1000
sample.ids<-sample(which(vols.char$df$good[1:32400]), sample.count)


x<-cbind(vols.char$df$PC01[sample.ids], vols.char$df$PC02[sample.ids])

n.grid<-5




pdfCluster<-function (x, graphtype, Q = "QJ", lambda = 0.1, grid.pairs = 10, 
    n.grid = min(50, nrow(as.matrix(x))), ...) 
{
    if (any(sapply(x[1, ], is.factor))) 
        stop("All variables must be numeric")
    if (any(is.na(x))) {
        warning(cat("NA in object", deparse(substitute(x)), "have been omitted", 
            "\n"))
        x <- na.omit(x)
    }
  	dots <- list()
    ndots <- names(dots)
    args.kepdf <- ndots %in% (names(formals(kepdf)))
    dots.kepdf <- dots[args.kepdf]
    args.pdfClassification <- ndots %in% (names(formals(pdfClassification)))
    dots.pdfClassification <- dots[args.pdfClassification]
    x <- data.matrix(x)
    #if (!"hmult" %in% ndots) 
    #    if (ncol(x) > 6) 
    #        dots$hmult = 1
    #    else 
			dots$hmult = 0.1
    #if ("h" %in% names(dots.kepdf)) 
    #    dots.kepdf$h <- dots.kepdf$h * dots$hmult
    #else 
		dots.kepdf$h <- dots$hmult * h.norm(x)

    pdf <- do.call(kepdf, c(list(x = x, eval.points = x), dots.kepdf))

    estimate <- pdf@estimate
    hasQ <- hasArg(Q)
    haslambda <- hasArg(lambda)
    hasgrid.pairs <- hasArg(grid.pairs)
    n <- nrow(x)
    d <- ncol(x)
    if (n.grid > n) {
        warning("n.grid too large, set equal to n")
        n.grid <- min(n.grid, nrow(x))
    }
    #if (missing(graphtype)) 
    #    if (d == 1) 
    #        graphtype = "unidimensional"
    #    else if (d <= 6) 
            graphtype = "delaunay"
    #    else graphtype = "pairs"
    graph.par <- list()
    # graph.par$type <- graphtype
    # if (NCOL(x) == 1) {
    #     if (hasQ) 
    #         message("Unused argument 'Q' when graphtype= 'unidimensional'")
    #     if (haslambda) 
    #         message("Unused argument 'lambda' when graphtype= 'unidimensional'")
    #     if (hasgrid.pairs) 
    #         message("Unused argument 'grid.pairs' when graphtype= 'unidimensional'")
    #     graph.nb <- graph.uni(x)
    # }
    # else if (graphtype == "delaunay") {
    #     if (haslambda) 
    #         message("Unused argument 'lambda' when graphtype= 'delaunay'")
    #     if (hasgrid.pairs) 
    #         message("Unused argument 'grid.pairs' when graphtype= 'delaunay'")
        graph.nb <- graph.del(x, Q = Q)
    # }
    # else if (graphtype == "pairs") {
    #     if (hasQ) 
    #         message("Unused argument 'Q' when graphtype= 'pairs'")
    #     graph.nb <- graph.pairs(x, pdf, lambda = lambda, grid.pairs = grid.pairs)
    #     graph.par$lambda = lambda
    #     graph.par$comp.area <- graph.nb$comp.areas
    # }
    # else stop("graphtype should be one of 'unidimensional', 'delaunay', or 'pairs'")
    nc <- num.con(x, estimate, graph.nb$graph, profile.grid = n.grid - 
        1, correct = TRUE)
    struct <- con2tree(nc, estimate)
    if (struct$bad) {
        message("No output given")
        message("The grid is too coarse: re-run with larger value of 'n.grid' or with larger bandwidth")
    }
    else {
        g <- struct$g
        g[struct$g == 0] <- NA
        pdf.comp <- list()
        pdf.comp$kernel <- pdf@kernel
        pdf.comp$bwtype <- pdf@bwtype
        pdf.comp$par <- pdf@par
        pdf.comp$estimate <- estimate
        out <- new("pdfCluster", call = match.call(), x = data.matrix(x), 
            pdf = pdf.comp, nc = nc, graph = graph.par, cluster.cores = g, 
            tree = struct$tree, noc = struct$noc, stages = NULL, 
            clusters = NULL)
        if ((!"n.stage" %in% ndots) || ("n.stage" %in% ndots && 
            dots.pdfClassification$n.stage > 0)) {
            out <- do.call(pdfClassification, c(list(obj = out), 
                dots.pdfClassification))
        }
        out
    }
}

con2tree<-function (object, f) 
{
    ow <- options("warn")
    nc <- object$nc # number of clusters for each grid
    p <- object$p # the grid borders 
    index <- which(diff(nc) != 0) # the grid id of nc which has increase or decrease
    K <- length(index) # number of inc/dec nc grids
    ps <- p[index] # each probability at inc/dec nc grids
    lista <- list()
    if (K == 1) {
        gruppi <- as.vector(object$id[, ncol(object$id)])
        M <- 1
    }
    else {
        for (j in (1:K)) lista[[j]] <- as.vector(object$id[, index[j]]) 
					# lista includes the cluster table for each index
        gruppi <- rep(0, length(lista[[1]]))
        M <- 0 # mode number (thefuture "number of cluster")
        insiemi <- list()
        k <- 1
        allocated <- (gruppi > 0) # the points which are already allocated to a cluster
        insiemi[[k]] <- setdiff(unique(gruppi), 0) # get the number of modes
        while (k < K) {
            k <- k + 1
            sets <- lista[[k]]
            insieme <- list()
            for (m in 1:max(sets)) { 
							# for each clusters detected in this probability
                set <- which(sets == m) # get the clustered points
                new <- setdiff(set, which(allocated)) 
									# check whether it is newly allocated dots or not
								print(paste("m=",m))
								print(paste("new=", toString(new)))
                if (length(new) > 0) {
                  g.new <- unique(gruppi[intersect(set, which(allocated))])
										# if the deteted cluster is growing, g.new will include
										# the cluster number of the previous prob. grid.
										# it may be a single value or multiple values.
									print(toString(g.new))
                  if (length(g.new) == 0) 
										# if there are no g.new, this is a new cluster!!
                    gruppi[set] <- M <- M + 1 # counting the "modes"
                  if (length(g.new) == 1) 
										# if g.new==1, the cluster growed without includeing
										# other clusters
                    gruppi[set] <- g.new
										# if there are multiple groups, no annotation!!
                  allocated <- (gruppi > 0)
                }
                gg <- sort(setdiff(unique(gruppi[set]), 0))
                if (length(gg) > 0) 
                  insieme[[length(insieme) + 1]] <- gg
            }
            insiemi[[k]] <- insieme
        }
        if (!missing(f)) {
            u <- unique(gruppi[rev(order(f))])
            g <- rep(0, length(f))
            u0 <- u[u > 0]
            for (i in 1:max(gruppi)) g[gruppi == u0[i]] <- i
            gruppi <- g
        }
    }

    salti <- diff(nc) # sulti means "jumps"
    salta.giu <- rev(which(diff(nc)[index] < 0)) # diff(nc)<0, decrasing modes
    altezza <- numeric(M) # altezza="height"
    m <- 0
    salti.su <- salti[salti > 0] # increasing planes's "increment of m"
    options(warn = -1)
    while (m < M) {
        m <- m + 1
        r <- min(which(cumsum(salti.su) >= m))
        altezza[m] <- p[salti > 0][r]
    }
    bad.grid <- any(is.na(altezza))
    sotto.albero <- function(tree, k, set) {
        insieme <- insiemi[[salta.giu[k]]]
        r <- 0
        branch <- list()
        for (item0 in insieme) {
            item <- intersect(set, unlist(item0))
            if (length(item) == 1) {
                r <- r + 1
                u <- item
                attr(u, "members") <- 1
                attr(u, "height") <- altezza[item]
                attr(u, "label") <- paste(as.character(item), 
                  " ", sep = "")
                attr(u, "leaf") <- TRUE
                branch[[r]] <- u
            }
            if (length(item) > 1) {
                r <- r + 1
                u <- sotto.albero(list(), k + 1, item)
                attr(u, "members") <- length(unlist(item))
                attr(u, "height") <- max(ps[salta.giu[k + 1]])
                attr(u, "label") <- paste("{", paste(item, collapse = ","), 
                  "}", sep = "")
                attr(u, "leaf") <- FALSE
                branch[[r]] <- u
            }
        }
        branch
    }
    if (M > 1) {
        tree <- sotto.albero(list(), 1, 1:M)
        attr(tree, "members") <- M
        attr(tree, "height") <- max(ps[salta.giu[1]])
        attr(tree, "label") <- paste("{", paste(1:M, collapse = ","), 
            "}", sep = "")
        noc <- M
    }
    else {
        tree <- list()
        tree[[1]] <- 1
        attr(tree[[1]], "members") <- 1
        attr(tree[[1]], "height") <- 0
        attr(tree[[1]], "label") <- "1"
        attr(tree[[1]], "leaf") <- TRUE
        attr(tree, "members") <- 1
        attr(tree, "height") <- 1
        attr(tree, "label") <- paste("{", paste(1, collapse = ","), 
            "}", sep = "")
        noc <- 1
    }
    tree <- list(tree)
    attr(tree, "members") <- M
    attr(tree, "height") <- 1
    attr(tree, "class") <- "dendrogram"
    options(warn = ow$warn)
    invisible(list(g = gruppi, tree = tree, bad = bad.grid, noc = noc))
}


num.con<-function (x, estimate, x.nb, pn = 0.9, profile = TRUE, profile.grid = 25, 
    correct = FALSE) 
{
    n <- length(estimate)
    ngrid <- profile.grid
    if (profile) 
        pn <- seq(0, 1, length = ngrid + 2)[-c(1, ngrid + 2)]
    qn <- as.numeric(quantile(estimate, 1 - pn))
    xd.id <- (matrix(estimate, ncol = 1) %*% (1/qn)) < 1
    nc.nc <- rep(0, length(qn))
    names(nc.nc) <- format(pn, digit = 3)
    nc.id <- matrix(0, nrow = n, ncol = length(qn))
    colnames(nc.id) <- format(pn, digit = 3)
    for (i in 1:length(qn)) {
        ni <- sum(xd.id[, i])
        if (ni < n) {
            x.nbc <- x.nb # the trialngulation connection data for each points
            ind <- which(!xd.id[, i])
            for (j in ind) x.nbc[[j]] <- intersect(x.nb[[j]], 
                ind)
            x.nbc[xd.id[, i]] <- as.integer(0)
            nc <- find.nc(x.nbc)
            nc.nc[i] <- nc$nc - ni
            nc.id[xd.id[, i], i] <- -1
            nc.id[!xd.id[, i], i] <- unclass(factor(nc$comp.id[!xd.id[, 
                i]]))
        }
        if (ni == n) {
            nc.nc[i] <- 0
            nc.id[, i] <- -1
        }
        if (correct) {
            tt <- table(nc.id[nc.id[, i] != -1, i])
            ttc <- as.integer(names(tt)[tt <= 1])
            ttp <- as.integer(names(tt)[tt > 1])
            if (length(ttc) > 0) 
                for (l in 1:length(ttc)) nc.id[nc.id[, i] == 
                  ttc[l], i] <- -1
            nc.nc[i] <- length(ttp)
            nc.nc[i] <- length(tt)
        }
        nc.nc[i] <- length(unique(nc.id[, i])) - 1
    }
    if (profile) {
        newnames <- c("0", names(nc.nc), "1")
        nc.nc <- c(0, nc.nc, 1)
        names(nc.nc) <- newnames
        nc.id <- cbind(rep(-1, nrow(nc.id)), nc.id, rep(1, nrow(nc.id)))
        pn <- c(0, pn, 1)
        qn <- c(max(estimate), qn, min(estimate))
    }
    list(nc = nc.nc, p = pn, id = nc.id, pq = cbind(p = pn, q = qn))
}

graph.del<-function (x, Q = "QJ") 
{
    x <- as.matrix(x)
    x0 <- x
    sd <- sqrt(diag(var(x)))
    m <- apply(x, 2, mean)
    x.scaled <- t((t(x) - m)/sd)
    ncx <- dim(x.scaled)[2]
    nrx <- dim(x.scaled)[1]
    if (Q == "QT") 
        xt <- delaunayn(x.scaled, options = "Qt")
    if (Q == "QJ") 
        xt <- delaunayn(x.scaled, options = "QJ")
    on.exit(unlink("qhull_out.txt"))
    x.nb <- vector(mode = "list", length = nrx)
    for (i in 1:nrx) {
        x.nb[[i]] <- as.integer(sort(unique(as.vector(xt[(xt == 
            i) %*% rep(1, ncx + 1) > 0, ]), FALSE)))
    }
    rownames <- as.character(1:length(x.nb))
    list(graph = x.nb, type = "delaunay")
}

find.nc<-function (nb.obj) 
{
    comp <- integer(length(nb.obj))
    comp <- .Call("g_components", nb.obj, as.integer(comp), PACKAGE = "pdfCluster")
    answ <- list(nc = length(unique(comp)), comp.id = comp)
    answ
}


