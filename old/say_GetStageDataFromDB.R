#
# say_GetStageDataFromDB.R
# 
# <Usage> R --vanilla < say_StageSleep.R --args "" "20111118.ID033572"

source("say.R")
args<-commandArgs(trailingOnly = TRUE)
experiment.name<-args[1]
mouse.id<-args[2]

print(GetStageDataFromDB(experiment.name, mouse.id))
