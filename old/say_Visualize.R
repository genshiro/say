#
# say_Visualize.R
# 
# <Usage> R --vanilla < say_Visualize.R --args 20111118.ID033572
#
#

source("say.R")
library(Cairo)
library("rgl")
library(grid)

args<-commandArgs(trailingOnly = TRUE)
mouse.ids<-args[c(-1,-2)]
drug.pattern<-args[1]
drug.type=args[2]

#mouse.ids<-c("20120201.ID048069","20120201.ID033572","20120201.ID051290") #MOD
#drug.pattern<-"ABB"
#drug.type="MOD"
#mouse.ids<-c("20111118.ID033572", "20111118.ID044502", "20111118.ID045831","20111118.ID051038") #B6J


for (m in mouse.ids){
	
	mouse.id<-m
	
	load(file.path("data","char", paste(mouse.id, "vols.char","xdr", sep=".")))
	load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
	auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
	#load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
	#manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

	# set the signes of PC
	pc1.nrem<-mean(vols.char$df$PC01[auto.stage==sleep.stage.labels[1]])
	vols.char$df$PC01<-vols.char$df$PC01*sign(pc1.nrem)
	vols.char$rotation[,1]<-vols.char$rotation[,1]*sign(pc1.nrem)

	pc2.rem<-mean(vols.char$df$PC02[auto.stage==sleep.stage.labels[2]])
	vols.char$df$PC02<-vols.char$df$PC02*sign(pc2.rem)
	vols.char$rotation[,2]<-vols.char$rotation[,2]*sign(pc2.rem)

	pc3.rem<-mean(vols.char$df$PC03[auto.stage==sleep.stage.labels[2]])
	vols.char$df$PC03<-vols.char$df$PC03*sign(pc3.rem)
	vols.char$rotation[,3]<-vols.char$rotation[,3]*sign(pc3.rem)

	pc4.rem<-mean(vols.char$df$PC04[auto.stage==sleep.stage.labels[2]])
	vols.char$df$PC04<-vols.char$df$PC04*sign(pc4.rem)
	vols.char$rotation[,4]<-vols.char$rotation[,4]*sign(pc4.rem)

	# set the ranges for PCs
	pc01.range<-quantile(vols.char$df$PC01, c(0.001,0.999), TRUE)
	pc02.range<-quantile(vols.char$df$PC02, c(0.001,0.999), TRUE)

	stage.color<-c("indianred1","darkolivegreen3","dodgerblue","black")
	stage.color.alpha<-c("#ff6a6a66","#a2cd5a66","#1e90ff66", "#00000066")

	.count.stages<-function(xxx){
		return(c(length(which(xxx==1)), length(which(xxx==2)), length(which(xxx==3)), length(which(xxx==4))))
	}

	#manual.stage.t<-t(apply(matrix(as.numeric(manual.stage), nrow=450), 2, .count.stages))
	auto.stage.t<-t(apply(matrix(as.numeric(auto.stage), nrow=450), 2, .count.stages))
	colnames(manual.stage.t)<-sleep.stage.labels
	colnames(auto.stage.t)<-sleep.stage.labels
	max.hour<-dim(auto.stage.t)[1]

	CairoFonts(
		regular="Arial:style=Regular",
		bold="Arial:style=Bold",
		italic="Arial:style=Italic",
		bolditalic="Arial:style=Bold Italic,BoldItalic",
		symbol="Symbol"
	)

	dir.create("figure", recursive=TRUE, showWarnings=FALSE) # make the folders before saving data

	# scatter plot PC01xPC02@auto.stage

	p<-ggplot(data.frame(PC01=vols.char$df$PC01, PC02=vols.char$df$PC02, stage=auto.stage))
	p<-p+geom_point(aes(x=PC01, y=PC02, color=stage), alpha=0.5, size=0.5)
	p<-p+scale_color_manual(breaks=levels(auto.stage), values=stage.color.alpha)
	p<-p+theme_minimal()
	p<-p+coord_cartesian(xlim=pc01.range, ylim=pc02.range)
	p<-p+labs(x="", y="")
	p<-p+theme(panel.margin = unit(2, "lines"), legend.position = "none", panel.grid=element_blank(), 
		axis.text=element_text(size = 53.33)) # this is adjusted for the paper	
	CairoPNG(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, "auto.stage", "png", sep=".")),
		width=960, height=960)
	print(p)
	dev.off()

	# scatter plot PC01xPC02@manual.stage

	# scatter plot PC01xPC02@auto.stage


	# time series plot PC01xPC02@auto.stage

	pdf.height<-c(6,2,6)
	ylim.max<-c(60,20,60)

	CairoPDF(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, "ts",
		"pdf", sep=".")), 64, 14, bg="transparent")

	layout(matrix(c(1,1,1,2,3,3,3), ncol=1, nrow=7))
	par(mar=c(1,1,0,0))


	for (i in 1:3){


		matplot(x=seq(max.hour), y=cbind(manual.stage.t[,sleep.stage.labels[i]],
			auto.stage.t[,sleep.stage.labels[i]])/450*60, type="l", xaxs="i", yaxs="i", 
			ylim=c(0,ylim.max[i]), lwd=3, lty=c(2,1), xaxt="n", axes=FALSE, 
			yaxt="n", col=c("Black", stage.color[i]))
		axis(1, at=c(0,max.hour), NA)
		axis(2, at=c(0, ylim.max[i]))

	}
	dev.off()
	
	# visualize eigen vectors
	dir.create("figure", recursive=TRUE, showWarnings=FALSE) # make the folders before saving data
	CairoPDF(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, "PC.eigenvectors", "pdf", sep=".")), width=12, 8, bg="transparent")
	layout(matrix(c(1:8), ncol=2, nrow=4))
	par(mar=c(1,2,0,0))

	for (i in 1:4){
		pcs=sprintf("PC%02d", i)
		y<-vols.char$rotation[1:400, i]
		plot(50*(1:400)/400, y, ylim=c(-.15,.15), col="dodgerblue", lty=1, type="l", axes=FALSE, ylab="", xlab="")
		lines(x=c(0,50), y=c(0,0), col = "black", lty=3)
		axis(1, (0:5)*10, NA)
		#axis(2, c((1:7)*0.05-0.2), c(NA,-0.1,NA,0,NA,0.1,NA))
		axis(2, c((1:7)*0.05-0.2), NA)
	}

	for (i in 1:4){
		pcs=sprintf("PC%02d", i)
		y<-vols.char$rotation[400+1:400, i]
		plot(50*(1:400)/400, y, ylim=c(-.15,.15), col="indianred1", lty=1, type="l", axes=FALSE, ylab="", xlab="")
		lines(x=c(0,50), y=c(0,0), col = "black", lty=3)
		axis(1, (0:5)*10, NA)
		#axis(2, c((1:7)*0.05-0.2), c(NA,-0.1,NA,0,NA,0.1,NA))
		axis(2, c((1:7)*0.05-0.2), NA)
	}

	dev.off()




	
	
	
}

# Basals
if (max.hour==144){

	light.df<-c(rep(c(rep("Light",12), rep("Dark",12)),3), rep("Dark", 72))

	# LD vs DD
	nrem.df<-data.frame()
	

	for (m in seq(length(mouse.ids))){ 
		mouse.id<-mouse.ids[m]

		load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
		auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
		load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
		manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

		manual.stage.t<-t(apply(matrix(as.numeric(manual.stage), nrow=450), 2, .count.stages))/450*60
		auto.stage.t<-t(apply(matrix(as.numeric(auto.stage), nrow=450), 2, .count.stages))/450*60
		colnames(manual.stage.t)<-sleep.stage.labels
		colnames(auto.stage.t)<-sleep.stage.labels
		
		mnt<-manual.stage.t[,1]
		ant<-auto.stage.t[,1]
		
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="0-12", type="Manual", light="LD",
			nrem.time=sum(mnt[which((0:71)%%24<12)])))
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="0-12", type="Manual", light="DD", 
			nrem.time=sum(mnt[72+which((0:71)%%24<12)])))
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="12-24", type="Manual", light="LD",
			nrem.time=sum(mnt[which((0:71)%%24>11)])))
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="12-24", type="Manual", light="DD",
			nrem.time=sum(mnt[72+which((0:71)%%24>11)])))

		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="0-12", type="Auto", light="LD",
			nrem.time=sum(ant[which((0:71)%%24<12)])))
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="0-12", type="Auto", light="DD", 
			nrem.time=sum(ant[72+which((0:71)%%24<12)])))
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="12-24", type="Auto", light="LD",
			nrem.time=sum(ant[which((0:71)%%24>11)])))
		nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, ZT="12-24", type="Auto", light="DD",
			nrem.time=sum(ant[72+which((0:71)%%24>11)])))


	}

	CairoPDF(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, "nrem_boxplot", "pdf", sep=".")), width=8, height=8, bg="transparent")

	p<-ggplot(nrem.df)
	p<-p+geom_boxplot(aes(x=ZT, y=nrem.time, fill=ZT), alpha=0.5, size=0.5)
	p<-p+scale_y_continuous(limits=c(0, 1500))
	p<-p+scale_fill_manual(values=c("0-12"="orange", "12-24"="grey"))
	p<-p+facet_grid(light~type)
	p<-p+theme_minimal()
	p<-p+labs(y="NREM time (m)")
	p<-p+theme(panel.margin = unit(2, "lines"), legend.position = "none", text=element_text(size = 36))	
	print(p)

	dev.off()


	CairoPDF(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, "nrem_boxplot_dd", "pdf", sep=".")), width=8, height=8, bg="transparent")

	p<-ggplot(subset(nrem.df, light=="DD"))
	p<-p+geom_boxplot(aes(x=ZT, y=nrem.time, fill=ZT), alpha=0.5, size=0.5)
	p<-p+scale_y_continuous(limits=c(0, 1500))
	p<-p+scale_fill_manual(values=c("0-12"="orange", "12-24"="grey"))
	p<-p+facet_grid(light~type)
	p<-p+theme_minimal()
	p<-p+labs(y="NREM time (m)")
	p<-p+theme(panel.margin = unit(2, "lines"), legend.position = "none", text=element_text(size = 36))	
	print(p)

	dev.off()


	zts<-c("0-12","12-24")
	lights<-c("LD","DD")
	types<-c("Manual","Auto")
	sum.df<-data.frame()
	
	for (z in zts){
		for (l in lights){
			for (t in types){
				df<-subset(nrem.df, (ZT==z)&(light==l)&(type==t))
				sum.df<-rbind(sum.df, data.frame(mean=mean(df$nrem.time), sd=sd(df$nrem.time), n=dim(df)[1],
					ZT=z, light=l, type=t, stringsAsFactors=FALSE))
			}
		}
	}
	
	write.csv(nrem.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem_raw", "csv", sep=".")))
	write.csv(sum.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem_sum", "csv", sep=".")))

	


	
	
	
	
}




# Drug vs VEH
if (max.hour==192){

	

	nrem.df<-data.frame()
	wake.df<-data.frame()

	if (drug.type=="MOD") {
		IP_time<-list(27, 27+24*4)
	}

	if (drug.type=="DIP") {
		IP_time<-list(38, 38+24*4)
	}

	for (m in seq(length(mouse.ids))){ 
		mouse.id<-mouse.ids[m]

		load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
		auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
		load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
		manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

		manual.stage.t<-t(apply(matrix(as.numeric(manual.stage), nrow=450), 2, .count.stages))/450*60
		auto.stage.t<-t(apply(matrix(as.numeric(auto.stage), nrow=450), 2, .count.stages))/450*60
		colnames(manual.stage.t)<-sleep.stage.labels
		colnames(auto.stage.t)<-sleep.stage.labels
		
		mnt<-manual.stage.t[,1]
		ant<-auto.stage.t[,1]
		mnt_w<-manual.stage.t[,3]
		ant_w<-auto.stage.t[,3]
		
		dp<-substr(drug.pattern,m,m)

		for (i in 1:2){
			
			IP<-IP_time[[i]]
			if (dp=="A") j<-i else j<-3-i # "A" is drug first, "B" is drug last group.
			drug<-c(drug.type, "VEH")[j]

			nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, drug=drug, type="Manual",
				duration="3h", nrem.time=sum(mnt[IP+(0:2)])))
			nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, drug=drug, type="Auto",
				duration="3h", nrem.time=sum(ant[IP+(0:2)])))

			nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, drug=drug, type="Manual",
				duration="6h", nrem.time=sum(mnt[IP+(0:5)])))
			nrem.df<-rbind(nrem.df, data.frame(mouse.id=m, drug=drug, type="Auto",
				duration="6h", nrem.time=sum(ant[IP+(0:5)])))

			wake.df<-rbind(wake.df, data.frame(mouse.id=m, drug=drug, type="Manual",
				duration="3h", wake.time=sum(mnt_w[IP+(0:2)])))
			wake.df<-rbind(wake.df, data.frame(mouse.id=m, drug=drug, type="Auto",
				duration="3h", wake.time=sum(ant_w[IP+(0:2)])))

			wake.df<-rbind(wake.df, data.frame(mouse.id=m, drug=drug, type="Manual",
				duration="6h", wake.time=sum(mnt_w[IP+(0:5)])))
			wake.df<-rbind(wake.df, data.frame(mouse.id=m, drug=drug, type="Auto",
				duration="6h", wake.time=sum(ant_w[IP+(0:5)])))

		}
	
		nrem.df$drug<-factor(nrem.df$drug, levels=c("VEH", drug.type))
		wake.df$drug<-factor(wake.df$drug, levels=c("VEH", drug.type))
	
	}


	dur<-c("3h", "6h")
	ymax<-c(150,300)
	for (d in 1:2){

		# NREM
		pdf(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, dur[d], "nrem_boxplot", "pdf", sep=".")), width=8, height=8, bg="transparent")

		p<-ggplot(subset(nrem.df, duration==dur[d]))
		p<-p+geom_boxplot(aes(x=drug, y=nrem.time, fill=drug), alpha=0.5, size=0.5)
		p<-p+scale_y_continuous(limits=c(0, ymax[d]))
		p<-p+scale_fill_manual(values=c("grey", "red"))
		p<-p+facet_grid(.~type)
		p<-p+theme_minimal()
		p<-p+labs(y="NREM time (m)", x="Drug types")
		p<-p+theme(panel.margin = unit(2, "lines"), legend.position = "none", text=element_text(size = 36))	
		print(p)

		dev.off()

		# Wake
		pdf(file.path("figure", paste(format(Sys.time(),"%Y%m%d%H%M%S"), mouse.id, dur[d], "wake_boxplot", "pdf", sep=".")), width=8, height=8, bg="transparent")

		p<-ggplot(subset(wake.df, duration==dur[d]))
		p<-p+geom_boxplot(aes(x=drug, y=wake.time, fill=drug), alpha=0.5, size=0.5)
		p<-p+scale_y_continuous(limits=c(0, ymax[d]))
		p<-p+scale_fill_manual(values=c("grey", "red"))
		p<-p+facet_grid(.~type)
		p<-p+theme_minimal()
		p<-p+labs(y="Wake time (m)", x="Drug types")
		p<-p+theme(panel.margin = unit(2, "lines"), legend.position = "none", text=element_text(size = 36))	
		print(p)

		dev.off()
		
	}









	dts<-c("VEH", drug.type)
	durs<-c("3h", "6h")
	types<-c("Manual","Auto")
	sum.nrem.df<-data.frame()
	sum.wake.df<-data.frame()
	
	for (d in dts){
		for (t in types){
			for (dur in durs){

				df<-subset(nrem.df, (drug==d)&(type==t)&(duration==dur))
				sum.nrem.df<-rbind(sum.nrem.df, data.frame(mean=mean(df$nrem.time), sd=sd(df$nrem.time), n=dim(df)[1],
					drug=d, type=t, duration=dur, stringsAsFactors=FALSE))

				df<-subset(wake.df, (drug==d)&(type==t)&(duration==dur))
				sum.wake.df<-rbind(sum.wake.df, data.frame(mean=mean(df$wake.time), sd=sd(df$wake.time), n=dim(df)[1],
					drug=d, type=t, duration=dur, stringsAsFactors=FALSE))

			}
		}
	}

	
	write.csv(nrem.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem_raw", "csv", sep=".")))
	write.csv(wake.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "wake_raw", "csv", sep=".")))
	write.csv(sum.nrem.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem_sum", "csv", sep=".")))
	write.csv(sum.wake.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "wake_sum", "csv", sep=".")))
	
	
	
	# compare the first 24 hours of each IP for test of FASTER
	faster.test.df<-data.frame()
	for (m in seq(length(mouse.ids))){ 

		mouse.id<-mouse.ids[m]

		load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
		auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
		load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
		manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

		faster.test.df<-rbind(faster.test.df, 
			data.frame(mouse.id=mouse.id, IP=1, CompareStages(auto.stage[1:10800], manual.stage[1:10800])))
		faster.test.df<-rbind(faster.test.df, 
			data.frame(mouse.id=mouse.id, IP=2, CompareStages(auto.stage[1:10800+43200], manual.stage[1:10800+43200])))

	}

	write.csv(faster.test.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "faster.test", "csv", sep=".")))


	# compare the first 24 hours of each IP for test of FASTER, 2-day-combined. 
	faster.test.df<-data.frame()
	for (m in seq(length(mouse.ids))){ 

		mouse.id<-mouse.ids[m]

		load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
		auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
		load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
		manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

		faster.test.df<-rbind(faster.test.df, data.frame(mouse.id=mouse.id, 
			CompareStages(auto.stage[c(1:10800, 1:10800+43200)], manual.stage[c(1:10800, 1:10800+43200)])))
		#faster.test.df<-rbind(faster.test.df, 
			#data.frame(mouse.id=mouse.id, IP=2, CompareStages(auto.stage[1:10800+43200], manual.stage[1:10800+43200])))

	}

	write.csv(faster.test.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "faster.test", "csv", sep=".")))

	
	
	
}




