#
# say_Visualize.R
# 
# <Usage> R --vanilla < say_Visualize.R --args 20111118.ID033572
#
#

source("say.R")
library(Cairo)
library("rgl")
library(grid)

args<-commandArgs(trailingOnly = TRUE)
mouse.ids<-args[c(-1,-2)]
drug.pattern<-args[1]
drug.type=args[2]

#mouse.ids<-c("20120201.ID048069","20120201.ID033572","20120201.ID051290") #MOD
#drug.pattern<-"ABB"
#drug.type="MOD"
#mouse.ids<-c("20111118.ID033572", "20111118.ID044502", "20111118.ID045831","20111118.ID051038") #B6J

#drug.pattern<-"DUM"
#drug.type="DUM"
#mouse.ids<-c("20121030.ID051290", "20121030.ID050851", "20111211.ID044497")

.count.stages<-function(xxx){
	return(c(length(which(xxx==1)), length(which(xxx==2)), length(which(xxx==3)), length(which(xxx==4))))
}

if (drug.type=="DUM") max.hour<-144 else max.hour<-168

# Basal

if (max.hour==144){

	wake.auto.df<-data.frame(time=1:12)
	nrem.auto.df<-data.frame(time=1:12)
	wake.manual.df<-data.frame(time=1:12)
	nrem.manual.df<-data.frame(time=1:12)

	for (m in seq(length(mouse.ids))){ 
		mouse.id<-mouse.ids[m]

		load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
		auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
		load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
		manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

		manual.stage.t<-t(apply(matrix(as.numeric(manual.stage), nrow=450*12), 2, .count.stages))/5400
		auto.stage.t<-t(apply(matrix(as.numeric(auto.stage), nrow=450*12), 2, .count.stages))/5400
		colnames(manual.stage.t)<-sleep.stage.labels
		colnames(auto.stage.t)<-sleep.stage.labels

		wake.auto.df<-data.frame(wake.auto.df, auto.stage.t[,"Wake"]/sum(auto.stage.t[1:6,"Wake"])*3)
		nrem.auto.df<-data.frame(nrem.auto.df, auto.stage.t[,"NREM"]/sum(auto.stage.t[1:6,"NREM"])*3)
		wake.manual.df<-data.frame(wake.manual.df, manual.stage.t[,"Wake"]/sum(manual.stage.t[1:6,"Wake"])*3)
		nrem.manual.df<-data.frame(nrem.manual.df, manual.stage.t[,"NREM"]/sum(manual.stage.t[1:6,"NREM"])*3)
		
	}
	
	colnames(wake.auto.df)<-c("Time", mouse.ids)
	colnames(nrem.auto.df)<-c("Time", mouse.ids)
	colnames(wake.manual.df)<-c("Time", mouse.ids)
	colnames(nrem.manual.df)<-c("Time", mouse.ids)
	
	write.csv(wake.auto.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "wake.auto", "csv", sep=".")))
	write.csv(nrem.auto.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem.auto", "csv", sep=".")))
	write.csv(wake.manual.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "wake.manual", "csv", sep=".")))
	write.csv(nrem.manual.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem.manual", "csv", sep=".")))
	
	t.test(x=colSums(nrem.auto.df[c(7,9,11),-1]), y=colSums(nrem.auto.df[c(8,10,12),-1]), paired=TRUE)
	t.test(x=colSums(nrem.manual.df[c(7,9,11),-1]), y=colSums(nrem.manual.df[c(8,10,12),-1]), paired=TRUE)
	
}




# Drug vs VEH
if (max.hour==192){

	wake.auto.df<-data.frame(time=1:192)
	nrem.auto.df<-data.frame(time=1:192)
	wake.manual.df<-data.frame(time=1:192)
	nrem.manual.df<-data.frame(time=1:192)

	for (m in seq(length(mouse.ids))){ 
		mouse.id<-mouse.ids[m]

		load(file.path("data","auto.stage", paste(mouse.id, "auto.stage.xdr", sep=".")))
		auto.stage<-factor(auto.stage$stage, levels=sleep.stage.labels)
		load(file.path("data","manual.stage", paste(mouse.id, "manual.stage.xdr", sep=".")))
		manual.stage<-factor(manual.stage$stage, levels=sleep.stage.labels) 

		manual.stage.t<-t(apply(matrix(as.numeric(manual.stage), nrow=450), 2, .count.stages))/450*60
		auto.stage.t<-t(apply(matrix(as.numeric(auto.stage), nrow=450), 2, .count.stages))/450*60
		colnames(manual.stage.t)<-sleep.stage.labels
		colnames(auto.stage.t)<-sleep.stage.labels

		wake.auto.df<-data.frame(wake.auto.df, auto.stage.t[,"Wake"])
		nrem.auto.df<-data.frame(nrem.auto.df, auto.stage.t[,"NREM"])
		wake.manual.df<-data.frame(wake.manual.df, manual.stage.t[,"Wake"])
		nrem.manual.df<-data.frame(nrem.manual.df, manual.stage.t[,"NREM"])
		
	}
	
	colnames(wake.auto.df)<-c("Time", mouse.ids)
	colnames(nrem.auto.df)<-c("Time", mouse.ids)
	colnames(wake.manual.df)<-c("Time", mouse.ids)
	colnames(nrem.manual.df)<-c("Time", mouse.ids)
	
	write.csv(wake.auto.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "wake.auto", "csv", sep=".")))
	write.csv(nrem.auto.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem.auto", "csv", sep=".")))
	write.csv(wake.manual.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "wake.manual", "csv", sep=".")))
	write.csv(nrem.manual.df, file.path("log", paste(format(Sys.time(),"%Y%m%d%H%M%S"), "nrem.manual", "csv", sep=".")))
	
}




