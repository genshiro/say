#
# say_ConvertDSIVolsToXdr.R
# 
# <Usage> R --vanilla < say_ConvertDSIVolsToXdr.R --args "20111118.ID033572"
#



source("say.R")
log.file<-gen.start.log("DSIVolsToXdr")
args<-commandArgs(trailingOnly = TRUE)
mouse.id<-args[1]
sampling.frequency<-100
delta.jst<-0
max.count<-0
header.start.datetime<-NA

if (length(args)>1) sampling.frequency<-as.numeric(args[2])
if (length(args)>2) delta.jst<-as.numeric(args[3])
if (length(args)>3) max.count<-as.numeric(args[4])
if (length(args)>4) header.start.datetime<-strptime(args[5], format="%Y%m%d%H%M%S")

print(ConvertDSIVolsToXdr(mouse.id, sampling.frequency, delta.jst, max.count, header.start.datetime, log.file))
